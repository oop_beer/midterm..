package com.sittipol.midtermm;

import java.util.Scanner;

public class PetShop {
    private String name;
    private String color;
    private String species;
    private double price;
    private int age ;


    public PetShop(String name, String color ,String species, double price, int age){
        this.name = name;
        this.species = species;
        this.color = color;
        this.price = price;
        this.age = age;
    }
    public PetShop(){

    }

    public String getName(){
        return name;
    }
    

    public String getColor(){
        return color;
    }
    public String getSpecies(){
        return species;
    }
    public double getPrice(){
        return price;
    }
    public int getAge(){
        return age;
    }
    public String toString(){
        return name+" "+"color: "+color+" "+"species: "+species+" "+"age: "+age;
    }


    }

    

