package com.sittipol.midtermm;

import java.util.Scanner;

public class SuperCarApp {

    public static void main(String[] args) {
        ShowSuperCar();

    }

    private static void ShowSuperCar() {
        Scanner sc = new Scanner(System.in);
        Showroom car1 = new Showroom("Bugatti Veyron", "Black", 297, 3.5);
        Showroom car2 = new Showroom("lamborghini aventador", "Yellow", 217, 0.5);
        Showroom car3 = new Showroom("Mclaren Senna", "Navy Blue", 208, 0.75);
        Showroom car4 = new Showroom("Hennessey Venom GT", "white", 270.49, 1.4);
        System.out.print("Enter Number:  ");
        int num = sc.nextInt();
        System.out.println("--------Check Detail--------");
        if (num == 1) {
            System.out.println("SuperCar1 Specs:  ");
            System.out.println(car1.toString());
            System.out.print("Buy (ํ1 or 2)"); // 1 = yes 2 = no
            int x = sc.nextInt();
            if (x == 1) {
                System.out.println(car1.getPrice());
                System.out.println("Thank you");
            } else if (x == 2) {
                System.out.println("I hope to use your services in the future.");

            }
        } else if (num == 2) {
            System.out.println("SuperCar2 Specs: ");
            System.out.println(car2.toString());
            System.out.print("Buy (1 or 2): "); // 1 = yes 2 = no
            int x = sc.nextInt();
            if (x == 1) {
                System.out.println(car2.getPrice());
                System.out.println("Thank you");
            } else if (x == 2) {
                System.out.println("I hope to use your services in the future.");

            }
        } else if (num == 3) {
            System.out.println("SuperCar3 Specs: ");
            System.out.println(car3.toString());
            System.out.print("Buy (1 or 2): "); // 1 = yes 2 = no
            int x = sc.nextInt();
            if (x == 1) {
                System.out.println(car3.getPrice());
                System.out.println("Thank you");
            } else if (x == 2) {
                System.out.println("I hope to use your services in the future.");

            }
        } else if (num == 4) {
            System.out.println("SuperCar4 Specs: ");
            System.out.println(car4.toString());
            System.out.print("Buy (1 or 2): "); // 1 = yes 2 = no
            int x = sc.nextInt();
            if (x == 1) {
                System.out.println(car4.getPrice());
                System.out.println("Thank you");
            } else if (x == 2) {
                System.out.println("I hope to use your services in the future.");

            }
        } else if (num == 5) {
            System.out.println("Show All Product");
            System.out.println(car1.toString());
            System.out.println(car2.toString());
            System.out.println(car3.toString());
            System.out.println(car4.toString());
        } else {
            System.out.println("Error");
        }
    }
}
