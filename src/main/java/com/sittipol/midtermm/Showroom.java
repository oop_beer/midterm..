package com.sittipol.midtermm;

public class Showroom {
    //Attribute
    private String name;
    private String color;
    private double topspeed;
    private double price;

    public Showroom(String name, String color , double topspeed ,double price) {
        this.name = name;
        this.color =color;
        this.topspeed = topspeed;
        this.price = price;//Constructor
    

    }
    //Method
    public String getName() {
        return name;
    }
    public String getColor(){
        return color;
    }
    public double getTopspeed(){
        return topspeed;
    }
    public String getPrice() {
        return price +" Milion Dollar";
    } 
    public String toString() {
        return "Brands: " +" "  + name +"\n"+"Color:  " + color +"\n"+"Top Speed: "+" "+topspeed+" mph "+"\n"+ "price: "+" "+ price+" "+"Milion Dollar";
    }
}
